import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Source {

	public static void main(String[] args) {
		new Idler();
		try{
			BufferedReader br = 
                    new BufferedReader(new InputStreamReader(System.in));
			
			String input;
				
			while((input=br.readLine())!=null){
				new Printer().print(input);
			}
			
		}catch(IOException io){
			io.printStackTrace();
		}
	}

}
