public class Printer {
	static{
		System.out.println("Static printers");
	}
	
	String ending = " jest smutny";
	
	{
		System.out.println("Creating printer");
	}
	
	public void print(String s){
		System.out.println(s+ending);
	}
}
